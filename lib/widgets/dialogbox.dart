import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

void dialogbox(BuildContext context, String title, String desc) {
  AwesomeDialog(
    context: context,
    animType: AnimType.scale,
    dialogType: DialogType.info,
    title: title,
    desc: desc,
    btnOkColor: Colors.purple,
    useRootNavigator: false,
    btnOkOnPress: () {},
  ).show();
}
