import '../pages/report_page.dart';
import '../pages/home_page.dart';
import 'package:flutter/material.dart';

int selectedIndex = 0;

onItemTapped(BuildContext context) {
  switch (selectedIndex) {
    case 0:
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const HomePage()),
      );
      break;
    case 1:
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const ReportPage()),
      );
      break;
  }
}

List<BottomNavigationBarItem> menuitems = [
  const BottomNavigationBarItem(
    icon: Icon(Icons.home),
    label: 'Home',
  ),
  const BottomNavigationBarItem(
    icon: Icon(Icons.report),
    label: 'Report',
  ),
];
