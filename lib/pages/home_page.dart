import '../model/cars.dart';
import '../widgets/bottomnavigationbar.dart';
import 'package:custom_bottom_sheet/custom_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io' show Platform;

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/services.dart';
import 'package:dropdown_search/dropdown_search.dart';
import '../widgets/search_box.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePagetate createState() => _HomePagetate();
}

class _HomePagetate extends State<HomePage> {
  ScanResult? scanResult;
  int searchmode = 0;
  List<bool> isSelected = [true, false];

  final _flashOnController = TextEditingController(text: 'Flash on');
  final _flashOffController = TextEditingController(text: 'Flash off');
  final _cancelController = TextEditingController(text: 'Cancel');

  var _aspectTolerance = 0.00;
  var _numberOfCameras = 0;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = [..._possibleFormats];

  @override
  void initState() {
    super.initState();

    if (cars.isEmpty) loadbackup();
    Future.delayed(Duration.zero, () async {
      _numberOfCameras = await BarcodeScanner.numberOfCameras;
      setState(() {});
    });
  }

  void onTabTapped(int index) {
    setState(() {
      selectedIndex = index;
      onItemTapped(context);
    });
  }

  final _textController = TextEditingController();

  void viewSettings(BuildContext context) {
    SlideDialog.showSlideDialog(
        context: context,
        backgroundColor: Colors.white,
        pillColor: Colors.deepPurple,
        transitionDuration: const Duration(milliseconds: 300),
        child: Expanded(
          child: ListView(
            shrinkWrap: true,
            controller: ScrollController(),
            children: <Widget>[
              const ListTile(
                title: Text('Camera selection'),
                dense: true,
                enabled: false,
              ),
              RadioListTile(
                onChanged: (v) => setState(() => _selectedCamera = -1),
                value: -1,
                title: const Text('Default camera'),
                groupValue: _selectedCamera,
              ),
              ...List.generate(
                _numberOfCameras,
                (i) => RadioListTile(
                  onChanged: (v) => setState(() => _selectedCamera = i),
                  value: i,
                  title: Text('Camera ${i + 1}'),
                  groupValue: _selectedCamera,
                ),
              ),
              const ListTile(
                title: Text('Button Texts'),
                dense: true,
                enabled: false,
              ),
              ListTile(
                title: TextField(
                  decoration: const InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    labelText: 'Flash On',
                  ),
                  controller: _flashOnController,
                ),
              ),
              ListTile(
                title: TextField(
                  decoration: const InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    labelText: 'Flash Off',
                  ),
                  controller: _flashOffController,
                ),
              ),
              ListTile(
                title: TextField(
                  decoration: const InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    labelText: 'Cancel',
                  ),
                  controller: _cancelController,
                ),
              ),
              if (Platform.isAndroid) ...[
                const ListTile(
                  title: Text('Android specific options'),
                  dense: true,
                  enabled: false,
                ),
                ListTile(
                  title: Text(
                    'Aspect tolerance (${_aspectTolerance.toStringAsFixed(2)})',
                  ),
                  subtitle: Slider(
                    min: -1,
                    value: _aspectTolerance,
                    onChanged: (value) {
                      setState(() {
                        _aspectTolerance = value;
                      });
                    },
                  ),
                ),
                CheckboxListTile(
                  title: const Text('Use autofocus'),
                  value: _useAutoFocus,
                  onChanged: (checked) {
                    setState(() {
                      _useAutoFocus = checked!;
                    });
                  },
                ),
              ],
              const ListTile(
                title: Text('Other options'),
                dense: true,
                enabled: false,
              ),
              CheckboxListTile(
                title: const Text('Start with flash'),
                value: _autoEnableFlash,
                onChanged: (checked) {
                  setState(() {
                    _autoEnableFlash = checked!;
                  });
                },
              ),
              const ListTile(
                title: Text('Barcode formats'),
                dense: true,
                enabled: false,
              ),
              ListTile(
                trailing: Checkbox(
                  tristate: true,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: selectedFormats.length == _possibleFormats.length
                      ? true
                      : selectedFormats.isEmpty
                          ? false
                          : null,
                  onChanged: (checked) {
                    setState(() {
                      selectedFormats = [
                        if (checked ?? false) ..._possibleFormats,
                      ];
                    });
                  },
                ),
                dense: true,
                enabled: false,
                title: const Text('Detect barcode formats'),
                subtitle: const Text(
                  'If all are unselected, all possible '
                  'platform formats will be used',
                ),
              ),
              ..._possibleFormats.map(
                (format) => CheckboxListTile(
                  value: selectedFormats.contains(format),
                  onChanged: (i) {
                    setState(
                      () => selectedFormats.contains(format)
                          ? selectedFormats.remove(format)
                          : selectedFormats.add(format),
                    );
                  },
                  title: Text(format.toString()),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    double colwidth = (MediaQuery.of(context).size.width * 0.75);

    final numberplate = TextFormField(
      controller: _textController,
      keyboardType: TextInputType.text,
      autofocus: false,
      onChanged: (text) {
        setState(() {
          currentCar.registrationNumber = text;
        });
      },
      decoration: InputDecoration(
        hintText: 'Number Plate',
        suffixIcon: IconButton(
          icon: const Icon(
            Icons.camera,
            color: Color(0xFF100887),
          ),
          tooltip: 'Scan',
          onPressed: _scan,
        ),
        labelText: 'Number Plate',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      initialValue: '',
      autofocus: false,
      onChanged: (text) {
        setState(() {
          currentCar.email = text;
        });
      },
      decoration: InputDecoration(
        hintText: 'Email',
        icon: const Icon(Icons.email),
        labelText: 'Email',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
    );

    final mobile = TextFormField(
      keyboardType: TextInputType.phone,
      initialValue: '',
      autofocus: false,
      onChanged: (text) {
        setState(() {
          currentCar.mobileNumber = text;
        });
      },
      decoration: InputDecoration(
        hintText: 'Mobile',
        icon: const Icon(Icons.phone),
        labelText: 'Mobile',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
    );

    final options = DropdownSearch<String>(
        popupProps: const PopupProps.dialog(
          showSelectedItems: true,
          showSearchBox: true,
        ),
        items: carWashOptions,
        dropdownDecoratorProps: const DropDownDecoratorProps(
          dropdownSearchDecoration: InputDecoration(
            hintText: 'Car Wash Option',
            labelText: 'Car Wash Option',
            contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
            border: OutlineInputBorder(),
          ),
        ),
        onChanged: (value) async {
          currentCar.washOption = value!;
        },
        selectedItem: currentCar.washOption);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Color(0xFF100887)),
        title: const SearchField(),
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            tooltip: 'Settings',
            onPressed: () {
              viewSettings(context);
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          save(context);
        },
        label: const Text('Save'),
        icon: const Icon(Icons.save),
        backgroundColor: const Color(0xFF100887),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 4.0),
          Image.asset(
            'assets/about.png',
            width: MediaQuery.of(context).size.width - 1,
          ),
          const SizedBox(height: 4.0),
          SizedBox(height: 45, width: colwidth, child: options),
          const SizedBox(height: 4.0),
          SizedBox(height: 45, width: colwidth, child: numberplate),
          const SizedBox(height: 4.0),
          SizedBox(height: 45, width: colwidth, child: email),
          const SizedBox(height: 4.0),
          SizedBox(height: 45, width: colwidth, child: mobile),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex:
            selectedIndex, // this will be set when a new tab is tapped
        selectedItemColor: Colors.lightBlueAccent,
        unselectedItemColor: const Color(0xFF100887),
        onTap: onTabTapped,
        items: menuitems,
      ),
    );
  }

  Future<void> _scan() async {
    try {
      final result = await BarcodeScanner.scan(
        options: ScanOptions(
          strings: {
            'cancel': _cancelController.text,
            'flash_on': _flashOnController.text,
            'flash_off': _flashOffController.text,
          },
          restrictFormat: selectedFormats,
          useCamera: _selectedCamera,
          autoEnableFlash: _autoEnableFlash,
          android: AndroidOptions(
            aspectTolerance: _aspectTolerance,
            useAutoFocus: _useAutoFocus,
          ),
        ),
      );
      setState(() {
        _textController.text = result.rawContent;
        scanResult = result;
      });
    } on PlatformException catch (e) {
      setState(() {
        scanResult = ScanResult(
          type: ResultType.Error,
          rawContent: e.code == BarcodeScanner.cameraAccessDenied
              ? 'The user did not grant the camera permission!'
              : 'Unknown error: $e',
        );
      });
    }
  }
}
