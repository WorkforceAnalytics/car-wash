import '../model/pie_chart.dart';
import '../widgets/dialogbox.dart';
import '../model/cars.dart';
import '../widgets/bottomnavigationbar.dart';
import 'package:flutter/material.dart';

import '../widgets/search_box.dart';

class ReportPage extends StatefulWidget {
  const ReportPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ReportPageState();
  }
}

class _ReportPageState extends State<ReportPage> {
  final Color barBackgroundColor = const Color(0xff72d8bf);
  final Duration animDuration = const Duration(milliseconds: 250);


  void onTabTapped(int index) {
    setState(() {
      selectedIndex = index;
      onItemTapped(context);
    });
  }

  @override
  void initState() {
    super.initState();
    loadPieChartData();
  }

  ListView history() {
    return ListView.builder(
        itemCount: cars.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(cars[index].washOption + ' : ' + cars[index].date,
                style: const TextStyle(color: Colors.indigo)),
            subtitle: Text(
                cars[index].registrationNumber +
                    ' : ' +
                    cars[index].mobileNumber,
                style: const TextStyle(color: Colors.indigo)),
            leading: const Icon(
              Icons.car_repair,
              color: Colors.indigo,
            ),
          );
        });
  }



  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
              IconButton(
                icon: const Icon(
                  Icons.backup,
                  color: Color(0xFF100887),
                ),
                onPressed: () async {
                  await mainBackup();
                  dialogbox(context, 'Car Wash', 'Backup Complete');
                },
              ),
              IconButton(
                icon: const Icon(
                  Icons.share,
                  color: Color(0xFF100887),
                ),
                onPressed: () async {
                  setState(() {
                    shareBackup();
                  });
                },
              ),
            ],
            title: const SearchField(),
            iconTheme: const IconThemeData(color: Color(0xFF100887)),
            backgroundColor: Colors.white,
            bottom: const TabBar(
              indicatorColor: Color(0xFF100887),
              unselectedLabelColor: Color(0xFF100887), // for unselected
              labelColor: Colors.lightBlueAccent, // for selected
              indicatorWeight: 5.0,
              tabs: [
                Tab(text: 'Log'),
                Tab(text: 'Dashboard'),
              ],
            ),
          ),
          backgroundColor: Colors.white,
          bottomNavigationBar: BottomNavigationBar(
            currentIndex:
                selectedIndex, // this will be set when a new tab is tapped
            selectedItemColor: Colors.lightBlueAccent,
            unselectedItemColor: const Color(0xFF100887),
            onTap: onTabTapped,
            items: menuitems,
          ),
          body: TabBarView(
            children: [
              SizedBox(height: double.infinity, child: history()),
              SizedBox(height: double.infinity, child: dashboard(context)),
            ],
          ),
        ));
  }




}
