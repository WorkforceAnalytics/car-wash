import '../model/cars.dart';
import 'package:flutter/material.dart';
import '../widgets/bottomnavigationbar.dart';
import '../widgets/search_box.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  void initState() {
    super.initState();
  }

  void onTabTapped(int index) {
    setState(() {
      selectedIndex = index;
      onItemTapped(context);
    });
  }

  ListView searchResults() {
    return ListView.builder(
        itemCount: searchresults.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
                searchresults[index].washOption +
                    ' : ' +
                    searchresults[index].date,
                style: const TextStyle(color: Colors.indigo)),
            subtitle: Text(
                searchresults[index].registrationNumber +
                    ' : ' +
                    searchresults[index].mobileNumber,
                style: const TextStyle(color: Colors.indigo)),
            leading: const Icon(
              Icons.car_repair,
              color: Colors.indigo,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: const IconThemeData(color: Color(0xFF100887)),
          title: const SearchField(),
          backgroundColor: Colors.white),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ListBody(
          children: <Widget>[
            Image.asset(
              'assets/about.png',
              width: MediaQuery.of(context).size.width - 1,
            ),
            const Divider(height: 5.0),
            SizedBox(height: 600, child: searchResults()),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex:
            selectedIndex, // this will be set when a new tab is tapped
        selectedItemColor: Colors.lightBlueAccent,
        unselectedItemColor: const Color(0xFF100887),
        onTap: onTabTapped,
        items: menuitems,
      ),
    );
  }
}
