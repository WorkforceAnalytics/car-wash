import 'package:car_wash/widgets/dialogbox.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';

class Car {
  String washOption;
  String date;
  String registrationNumber;
  String mobileNumber;
  String email;

  Car({
    required this.washOption,
    required this.date,
    required this.registrationNumber,
    required this.mobileNumber,
    required this.email,
  });

  factory Car.fromJson(Map<String, dynamic> json) {
    return Car(
        washOption: json['washOption'] ?? json['washOption'],
        date: json['date'] ?? json['date'],
        registrationNumber:
            json['registrationNumber'] ?? json['registrationNumber'],
        mobileNumber: json['mobileNumber'] ?? json['mobileNumber'],
        email: json['email'] ?? json['email']);
  }

  Map<String, dynamic> toJson() => {
        'washOption': washOption,
        'date': date,
        'registrationNumber': registrationNumber,
        'mobileNumber': mobileNumber,
        'email': email,
      };
}

List<Car> cars = [];
List<Car> searchresults = [];
Car currentCar = Car(
    washOption: 'Wash & Go',
    date: '',
    registrationNumber: '',
    mobileNumber: 'N/A',
    email: 'N/A');

List<String> carWashOptions = [
  'Wash & Go',
  'Wash & Dry',
  'Wash & Dry & Tyres',
  'Basic Wash (dry, vacuum, windows)',
  'Super Wash(basic & tyre shine)',
  'Wash & Wax',
  'Super wash & wax',
  'Custom Wash (super wash & engine)',
  'Premium Wash (super wash & roof lining)',
  'Mini Valet excludes engine & polish',
  'Full Valet'
];

List<String> days = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

final List<Color> availableColors = [
  Colors.purpleAccent,
  Colors.yellow,
  Colors.lightBlue,
  Colors.orange,
  Colors.pink,
  Colors.redAccent,
];

List<Car> search(String pattern) {
  return cars
      .where((c) => (c.registrationNumber == pattern ||
          c.mobileNumber == pattern ||
          c.email == pattern))
      .toList();
}

void save(BuildContext context) {
  if (currentCar.registrationNumber.isEmpty) {
    dialogbox(context, 'Car Wash', 'Invalid Number Plate');
    return;
  }

  var inputFormat = DateFormat('dd/MM/yyyy HH:mm');
  currentCar.date = inputFormat.format(DateTime.now()).toString();

  cars.add(currentCar);
  currentCar = Car(
      washOption: '',
      date: '',
      registrationNumber: '',
      mobileNumber: 'N/A',
      email: 'N/A');

  dialogbox(context, 'Car Wash', 'Transaction Saved');
  backup();
}

backup() async {
  try {
    await logresponse(jsonEncode(cars), 'cars.json');
  } on Exception catch (e) {
    debugPrint(e.toString());
  }
}

mainBackup() async {
  try {
    await logresponse(jsonEncode(cars), 'car_wash.json');
  } on Exception catch (e) {
    debugPrint(e.toString());
  }
}

shareBackup() async {
  try {
    final applicationDirectory = await getApplicationDocumentsDirectory();
    XFile file = XFile(applicationDirectory.path + '/car_wash.json');

    await logresponse(jsonEncode(cars), 'car_wash.json');
    Share.shareXFiles([file], text: 'Car Wash Logs');
  } on Exception catch (e) {
    debugPrint(e.toString());
  }
}

loadbackup() async {
  try {
    final applicationDirectory = await getApplicationDocumentsDirectory();
    String path = applicationDirectory.path;
    File file = File(path + '/cars.json');
    if (await file.exists()) {
      String contents = await file.readAsString();
      final parsed = json.decode(contents).cast<Map<String, dynamic>>();
      cars = parsed.map<Car>((json) => Car.fromJson(json)).toList();
    }
  } on Exception catch (e) {
    debugPrint(e.toString());
  }
}

logresponse(String responseBody, String filename) async {
  try {
    final applicationDirectory = await getApplicationDocumentsDirectory();
    String path = applicationDirectory.path;

    File(path + '/' + filename).writeAsString(responseBody);
  } on Exception catch (e) {
    debugPrint(e.toString());
  }
}
