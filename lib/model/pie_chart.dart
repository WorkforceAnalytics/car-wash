import 'package:pie_chart/pie_chart.dart';
import 'package:flutter/material.dart';

import 'cars.dart';

Map<String, double> dataMap =  {};

loadPieChartData()
{
     for (String item in carWashOptions) {
      int count = cars.where((c) => c.washOption == item).toList().length;
      dataMap.putIfAbsent(item, () => double.parse(count.toString()));
    }
}

PieChart dashboard(BuildContext context) {
  return PieChart(
    dataMap: dataMap,
    animationDuration: const Duration(milliseconds: 1000),
    chartLegendSpacing: 32.0,
    chartRadius: MediaQuery.of(context).size.width / 1.2,
    chartType: ChartType.disc,
    initialAngleInDegree: 0,
    centerText: 'Car Wash',
    legendOptions: const LegendOptions(
      showLegendsInRow: false,
      legendPosition: LegendPosition.bottom,
      showLegends: true,
      legendShape: BoxShape.circle,
      legendTextStyle: TextStyle(
        fontWeight: FontWeight.bold,
      ),
    ),
    chartValuesOptions: const ChartValuesOptions(
      showChartValueBackground: true,
      showChartValues: true,
      showChartValuesInPercentage: false,
      showChartValuesOutside: false,
      decimalPlaces: 1,
    ),
  );
}